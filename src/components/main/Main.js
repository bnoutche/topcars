import React from 'react';
import { NavLink } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ReceiptIcon from '@material-ui/icons/Receipt';
import DescriptionIcon from '@material-ui/icons/Description';
import { Typography } from '@material-ui/core';

const Main = () => {
    return (
        <Grid
            style={{ height: '80vh' }}
            container
            justify='center'
            alignItems='center'
        >
            <Grid item lg={4}>
                <Box m={2} p={2}>
                    <Grid container spacing={2} justify='center'>
                        <Grid item xs={10}>
                            <Button
                                to='/rentals'
                                component={NavLink}
                                variant='contained'
                                fullWidth
                                color='primary'
                                startIcon={<ReceiptIcon />}
                            >
                                <Typography variant='h5' component='h4'>
                                    Rentals
                                </Typography>
                            </Button>
                        </Grid>
                        <Grid item xs={10}>
                            <Button
                                to='/invoices'
                                component={NavLink}
                                variant='contained'
                                fullWidth
                                color='primary'
                                startIcon={<DescriptionIcon />}
                            >
                                <Typography variant='h5' component='h4'>
                                    Invoices
                                </Typography>
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            </Grid>
        </Grid>
    );
};

export default Main;
