import React from 'react';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Grid, Box, Typography, Button } from '@material-ui/core';

const InputComponent = ({ inputRef, ...other }) => <div {...other} />;

const OutlinedDiv = ({ children, label }) => {
    return (
        <TextField
            variant='outlined'
            label={label}
            multiline
            InputLabelProps={{ shrink: true }}
            InputProps={{
                inputComponent: InputComponent,
            }}
            inputProps={{ children: children }}
        />
    );
};

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));

export default function BasicTextFields() {
    const classes = useStyles();

    return (
        <form noValidate autoComplete='off'>
            <Box m={2} p={2}>
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={4}>
                        <Grid container direction='column' spacing={1}>
                            <Grid item xs={12}>
                                <Typography variant='h6' component='h6'>
                                    New Rental
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Contract number'
                                    name='contractNumber'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Date'
                                    name='date'
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} lg={4}>
                        <Grid container direction='column' spacing={1}>
                            <Grid item xs={12}>
                                <Typography variant='h6' component='h6'>
                                    Personal Information
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='ID-card'
                                    name='idCard'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Social security number'
                                    name='socialSecurityNumber'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Last name'
                                    name='lastName'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='First name'
                                    name='firstName'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Street'
                                    name='street'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='House number'
                                    name='houseNumber'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Zip code'
                                    name='zipCode'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='City'
                                    name='city'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Tel/GSM'
                                    name='telGSM'
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} lg={4}>
                        <Grid container direction='column' spacing={1}>
                            <Grid item xs={12}>
                                <Typography variant='h6' component='h6'>
                                    Vehicle
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Brand'
                                    name='brand'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Type'
                                    name='type'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Plate number'
                                    name='plateNumber'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Box mt={2}>
                                    <Typography variant='h6' component='h6'>
                                        Period
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Begin date'
                                    name='beginDate'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='End date'
                                    name='endDate'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Pickup date'
                                    name='pickupDate'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Return date'
                                    name='returnDate'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Arrival date'
                                    name='arrivalDate'
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Box mt={2}>
                            <Grid container justify='flex-end'>
                                <Grid item xs={2}>
                                    <Button
                                        component={NavLink}
                                        to='/rentals/driver/new'
                                        fullWidth
                                        color='primary'
                                        variant='contained'
                                    >
                                        Next
                                    </Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </form>
    );
}
