import React from 'react';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Grid, Box, Typography, Button } from '@material-ui/core';

export default function BasicTextFields() {
    return (
        <form noValidate autoComplete='off'>
            <Box m={2} p={2}>
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={4}>
                        <Grid container direction='column' spacing={1}>
                            <Grid item xs={12}>
                                <Typography variant='h6' component='h6'>
                                    Insurance Information
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Certain exemption'
                                    name='certainExemption'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={6}>
                                        <TextField
                                            fullWidth
                                            id='standard-basic'
                                            label='Depannage'
                                            name='idCard'
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextField
                                            fullWidth
                                            id='standard-basic'
                                            label='Price'
                                            name='Price'
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Date'
                                    name='date'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Amount'
                                    name='amount'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Payment method'
                                    name='paymentMethod'
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Box mt={2}>
                            <Grid container justify='flex-end'>
                                <Grid item xs={2}>
                                    <Button
                                        component={NavLink}
                                        to='/rentals/insurance/new'
                                        fullWidth
                                        color='primary'
                                        variant='contained'
                                    >
                                        Save
                                    </Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </form>
    );
}
