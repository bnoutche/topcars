import React from 'react';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Grid, Box, Typography, Button } from '@material-ui/core';

export default function BasicTextFields() {
    return (
        <form noValidate autoComplete='off'>
            <Box m={2} p={2}>
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={4}>
                        <Grid container direction='column' spacing={1}>
                            <Grid item xs={12}>
                                <Typography variant='h6' component='h6'>
                                    Driver Information
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Last name'
                                    name='lastName'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='First name'
                                    name='firstName'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Street'
                                    name='street'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='House number'
                                    name='houseNumber'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Zip code'
                                    name='zipCode'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='City'
                                    name='city'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='standard-basic'
                                    label='Tel/GSM'
                                    name='telGSM'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={6}>
                                        <TextField
                                            fullWidth
                                            id='standard-basic'
                                            label='ID-card'
                                            name='idCard'
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextField
                                            fullWidth
                                            id='standard-basic'
                                            label='Expiration date'
                                            name='expirationDate'
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={6}>
                                        <TextField
                                            fullWidth
                                            id='standard-basic'
                                            label='Social security number'
                                            name='socialSecurityNumber'
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextField
                                            fullWidth
                                            id='standard-basic'
                                            label='Date of birth'
                                            name='dateOfBirth'
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={6}>
                                        <TextField
                                            fullWidth
                                            id='standard-basic'
                                            label='License category'
                                            name='licenseCategory'
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextField
                                            fullWidth
                                            id='standard-basic'
                                            label='Validity period'
                                            name='validityPeriod'
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Box mt={2}>
                            <Grid container justify='flex-end'>
                                <Grid item xs={2}>
                                    <Button
                                        component={NavLink}
                                        to='/rentals/insurance/new'
                                        fullWidth
                                        color='primary'
                                        variant='contained'
                                    >
                                        Next
                                    </Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </form>
    );
}
