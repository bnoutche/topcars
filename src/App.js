import React from 'react';
import Layout from 'shared/Layout';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from 'shared/AppBar';
import AppRoutes from 'routes/AppRoutes';
import { Box, Grid, Paper } from '@material-ui/core';

function App() {
    return (
        <Layout>
            <AppRoutes />
        </Layout>
    );
}

export default App;
