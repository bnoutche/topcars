import Rentals from 'components/rentals';
import NewTenantForm from 'components/rentals/form/tenant/new';
import NewDriverForm from 'components/rentals/form/driver/new';
import NewInsuranceForm from 'components/rentals/form/insurance/new';
import { mainPaths } from '../main';

export const rentalsPaths = {
    rentals: `${mainPaths.main}rentals`,
    newRentals: `${mainPaths.main}rentals/tenants/new`,
    newDriverRentals: `${mainPaths.main}rentals/driver/new`,
    newInsuranceRentals: `${mainPaths.main}rentals/insurance/new`,
};

export const RENTALS_ROUTES = [
    {
        path: rentalsPaths.rentals,
        exact: true,
        component: Rentals,
    },
    {
        path: rentalsPaths.newRentals,
        exact: true,
        component: NewTenantForm,
    },
    {
        path: rentalsPaths.newDriverRentals,
        exact: true,
        component: NewDriverForm,
    },
    {
        path: rentalsPaths.newInsuranceRentals,
        exact: true,
        component: NewInsuranceForm,
    },
];
