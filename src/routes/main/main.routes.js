import Main from 'components/main';

const main = '/';

export const mainPaths = {
    main: `${main}`,
};

export const MAIN_ROUTES = [
    {
        path: mainPaths.main,
        exact: true,
        component: Main,
    },
];
