import React from 'react';
import { Switch } from 'react-router-dom';
import { MAIN_ROUTES } from './main';
import { RENTALS_ROUTES } from './rentals';
import RenderRoutes from './RenderRoutes';

const routes = [...MAIN_ROUTES, ...RENTALS_ROUTES];
export const AppRoutes = () => {
    return (
        <Switch>
            {routes.map((route, index) => (
                <RenderRoutes
                    key={index}
                    component={route.component}
                    exact={route.exact}
                    path={route.path}
                />
            ))}
        </Switch>
    );
};

export default React.memo(AppRoutes);
