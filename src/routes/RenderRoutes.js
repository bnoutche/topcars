import React from 'react';
import { Route } from 'react-router-dom';
import { any } from 'prop-types';

export const RenderRoutes = ({ component: Component, ...rest }) => {
    debugger; //eslint-disable-line
    return (
        <Route
            render={(props) => {
                return <Component {...props} />;
            }}
            {...rest}
        />
    );
};

RenderRoutes.propTypes = {
    component: any,
};

export default React.memo(RenderRoutes);
