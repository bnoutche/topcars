import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
    palette: {
        common: { black: '#000', white: '#fff' },
        background: { paper: '#fff', default: '#fafafa' },
        primary: {
            light: 'rgba(155, 155, 155, 1)',
            main: 'rgba(0, 0, 0, 1)',
            dark: 'rgba(0, 0, 0, 1)',
            contrastText: '#fff',
        },
        secondary: {
            light: 'rgba(255, 255, 255, 1)',
            main: 'rgba(237, 237, 237, 1)',
            dark: 'rgba(255, 255, 255, 1)',
            contrastText: 'rgba(0, 0, 0, 1)',
        },
        error: {
            light: '#e57373',
            main: '#f44336',
            dark: '#d32f2f',
            contrastText: '#fff',
        },
        text: {
            primary: 'rgba(0, 0, 0, 1)',
            secondary: 'rgba(0, 0, 0, 1)',
            disabled: 'rgba(0, 0, 0, 0.38)',
            hint: 'rgba(0, 0, 0, 0.38)',
        },
    },
});
