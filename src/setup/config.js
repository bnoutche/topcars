import { createBrowserHistory } from 'history';

export const APP_DOMAIN = '/';

export const history = createBrowserHistory({
    basename: APP_DOMAIN,
});
