import React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import AppBar from 'shared/AppBar';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';

import theme from 'setup/theme';

const Layout = ({ children }) => {
    return (
        <MuiThemeProvider theme={theme}>
            <CssBaseline />
            <AppBar />
            <Grid container>
                <Grid item xs={4} lg={2}>
                    <Paper square style={{ minHeight: 'calc(94vh - 9px)' }} />
                </Grid>
                <Grid item xs={8} lg={10}>
                    <Box m={2}>
                        <Paper square style={{ minHeight: '80vh' }}>
                            {children}
                        </Paper>
                    </Box>
                </Grid>
            </Grid>
        </MuiThemeProvider>
    );
};

export default Layout;
