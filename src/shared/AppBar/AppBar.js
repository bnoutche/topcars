import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import MUIAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
}));

const AppBar = () => {
    const classes = useStyles();
    const history = useHistory();

    const handleOnClick = useCallback(() => {
        history.push('/');
    }, []);

    return (
        <MUIAppBar position='static'>
            <Toolbar variant='dense'>
                <IconButton className={classes.icon} onClick={handleOnClick}>
                    <Avatar
                        src={process.env.PUBLIC_URL + '/topcars-logo.png'}
                    />
                </IconButton>
            </Toolbar>
        </MUIAppBar>
    );
};

export default AppBar;
